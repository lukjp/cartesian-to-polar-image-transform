'''
Transform an image where cartesian coordinates on the new image corresponds to
polar coordinates on the original Image
'''
from PIL import Image
import numpy as np
from math import sin,cos,tau
import cmath
import sys

# center of the Gear in pixels (right, down)
Center = np.array([1070, 1064])

# minimal and maximal radius to appear on the image in per cent of the maximum
# possible Radius completely inside the Image
rMinPerCent = 10
rMaxPerCent = 100


# open Image and store in numpy array with axes (right, down, RGB)
origImg = Image.open(sys.argv[1])
dataOrig = np.array(origImg)
dataOrig = np.transpose(dataOrig, (1, 0, 2))

# calculate maximum and minimum radius in pixel
sizeOrig = np.array( [len(dataOrig[0]), len(dataOrig)] )
maxR = min( ( min(Center - 1), min(sizeOrig - Center - 1) ) )
minR = int(round(maxR * rMinPerCent / 100))
maxR = int(round(maxR * rMaxPerCent / 100))

# we scale so that the outer row is not rescaled or
dataPolar = np.zeros( (maxR - minR, int(round(maxR * tau)), 3), dtype=np.uint8 )

# calculate pixel for pixel by reading the vale at corresponding position in
# the original image
for r in range(minR, maxR):
    for phi in range(len(dataPolar[0])):
        x = int(round(r * cos(phi * tau / len(dataPolar[0]) ) ) ) + Center[0]
        y = int(round(r * sin(phi * tau / len(dataPolar[0]) ) ) ) + Center[1]
        dataPolar[r - minR , phi] = dataOrig[x, y]

# save back image with appendix
imgPolar = Image.fromarray(dataPolar, 'RGB')
imgPolar.save(sys.argv[1] + '.polar.png')
